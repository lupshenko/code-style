# PROJECT MOVED

to [this page] on my website.

[this page]: https://kirby.kevinson.org/about/rules-i-live-by/

---

(This is the version 2.0.1 of the document.)

These rules are supposed to be followed when commiting to my projects.
Considering I'm the only one who does that, it doesn't make much
sense, but, in any case, it's always good to at least have them
documented.

* Be simple and straightforward.
* Be consistent with the rest of the code and don't fuck everything
  up.
* Obey naming conventions and common code practices appropriate for
  the language.
* Use indentation and whitespacing like a normal human being and keep
  lines short to be easily readable.
* Don't use any encodings other than UTF-8 and line endings other than
  LF.
* Don't use any date formats other than ISO 8601, languages other than
  North American English, and measurement systems other than metric
  unless the user is supposed to have control over them and, in case
  the user does, use the aforementioned parameters by default.
* Violate any rule if it's technically impossible to follow.

## Bonus

* Use tabs instead of spaces.
* Don't do any vertical alignment.
* Use kebab-case for any user-exposed interface (file/directory names,
  CLI commands, etc).
